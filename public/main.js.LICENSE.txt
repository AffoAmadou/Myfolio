/*!
 * GSAP 3.11.3
 * https://greensock.com
 *
 * @license Copyright 2008-2022, GreenSock. All rights reserved.
 * Subject to the terms at https://greensock.com/standard-license or for
 * Club GreenSock members, the agreement issued with that membership.
 * @author: Jack Doyle, jack@greensock.com
*/

/**
 * @license
 * Lodash <https://lodash.com/>
 * Copyright OpenJS Foundation and other contributors <https://openjsf.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/**
 * Checks if an event is supported in the current execution environment.
 *
 * NOTE: This will not work correctly for non-generic events such as `change`,
 * `reset`, `load`, `error`, and `select`.
 *
 * Borrows from Modernizr.
 *
 * @param {string} eventNameSuffix Event name, e.g. "click".
 * @param {?boolean} capture Check if the capture phase is supported.
 * @return {boolean} True if the event is supported.
 * @internal
 * @license Modernizr 3.0.0pre (Custom Build) | MIT
 */

//! .about ad esempio passato da una delle pagine  

//! E lo inserisco nel content della pagina in cui sono ora

//! E percio sapere in che pagina mi trovo

//! Importo le diverse pagine sapendo che all'interno ci sono degli index quindi

//! Nessun bisogno di chiamare il file intero

//! Questo metodo mi permette di creare il content e cosi da recuperare il data-template 

//! Recupero solo il .content che contiene la parte di divs che cambia in ogni pagina

//! Recupero tutti i link della pagina 

//! Se ce né piu di uno, ad esempio le immagini faccio un selector all per avere un array

//! Sono tutte le classi che passo dalla classe pagina  ad esempio

//! una sola volta senza bisogno di chiamarli in tutte le pagine

//!200 = pagina ben caricata

//!CREARE LE IMMAGINI DOPO CAMBIO PAGINA

//!Creo sta classe per avere un punto in commune in tutte le pagine cosi da avere la possibilità di chiamare i metodi 

//!Creo una div per metterci la parte del "html" che voglio

//!Riassegno la pagina

//!all click non eseguo il cambio di pagina come dovrebbe esserer

//!cosi da non mettere anche i doctype etc 

//!i bottoni o altro in modo da poter fare le animazioni, entrate o uscite

//!recupero il contenuto della pagina
